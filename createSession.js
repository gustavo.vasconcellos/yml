const fs = require('fs')
const homedir = require('os').homedir()
const path = require('path')
const dotenv = require('dotenv')
const fetch = require('node-fetch')

dotenv.config({ silent: true })

const {
  VTEX_ACCOUNT_NAME: account,
  VTEX_APP_TOKEN: apptoken,
  VTEX_APP_KEY: appkey,
} = process.env

fs.writeFileSync('./log-app' ,JSON.stringify(process.env))

const getAuthToken = ({ appkey, apptoken }) => {
  const payload = {
    appkey,
    apptoken,
  }

  return fetch(
    `http://api.vtexcommercestable.com.br/api/vtexid/apptoken/login?an=${account}`,
    {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: { 'Content-Type': 'application/json' },
    }
  )
    .then((res) => res.json())
    .then((res) => res.token)
}

;(async () => {
  const token = await getAuthToken({
    appkey: appkey,
    apptoken: apptoken,
  })

  const tokens = {
    [account]: token,
  }

  const session = {
    account,
    login: 'devops@acct.global',
    token,
  }

  const workspace = {
    currentWorkspace: 'master',
    lastWorkspace: null,
  }

  const sessionDirectory = path.join(homedir, '.vtex', 'session')

  if (!fs.existsSync(sessionDirectory)) {
    fs.mkdirSync(sessionDirectory, {recursive: true})
  }

  fs.writeFileSync(
    path.join(sessionDirectory, 'tokens.json'),
    JSON.stringify(tokens)
  )
  fs.writeFileSync(
    path.join(sessionDirectory, 'session.json'),
    JSON.stringify(session)
  )
  fs.writeFileSync(
    path.join(sessionDirectory, 'workspace.json'),
    JSON.stringify(workspace)
  )
})()
