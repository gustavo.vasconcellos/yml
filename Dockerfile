FROM node:14.17.2 
WORKDIR .

ARG VTEX_ACCOUNT_NAME
ARG VTEX_APP_KEY
ARG VTEX_APP_TOKEN

ENV ACCOUNT_NAME=$VTEX_ACCOUNT_NAME
ENV APP_KEY=$VTEX_APP_KEY
ENV APP_TOKEN=$VTEX_APP_TOKEN

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install

RUN npm i -g vtex

COPY . .

RUN node createSession
